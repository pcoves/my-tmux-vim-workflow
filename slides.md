---
title: "TMux :heart: Neovim"
subtitle: A match made in heaven
author: "[Pablo COVES](https://pcoves.gitlab.io)"
date: Décembre 2022
---

# Aperçu

Où, quoi, quand, comment?

## [`TMux`][tmux]

Un *multiplexer* de terminal qu'il est bien

## [`NVim`][nvim]

Un éditeur basé sur [`Vim`][vim] qu'il est bien

## Ensemble

* Un *workflow* efficace, local ou distant
* Robuste aux redémarrages et déconnexions
* Incroyablement flexible et configurable
* Moderne et dynamique comme un IDE

## :warning:

Des exemples de configurations sont partagés dans cette présentation.

Copier-coller revient à s'imposer les choix d'autrui.

Mieux vaut les comprendre et s'en inspirer que les appliquer aveuglément.

# [`TMux`][tmux]

* Multi-session
* Multi-window
* Multi-pane
* Extensible

## Configuration

La configuration de [`TMux`][tmux] va dans

`~/.tmux.conf`

> Un exemple accessible [ici](https://pcoves/tmux)

## *Prefix*

* [`TMux`][tmux] se contrôle au clavier via un `Prefix`
* Par défaut, le `Prefix` est `<C-b>`
* Il est possible de [le changer](https://gitlab.com/pcoves/tmux/blob/master/.tmux.conf#L2-4)
* `Prefix` + `Commande` dirigent [`TMux`][tmux]

## Sessions

Toute action [`TMux`][tmux] s'effectue dans une **session**

### Nouvelle **session**

```bash
tmux
```

### Lister les **sessions**

::: columns
:::: {.column width="50%"}
Dans le terminal

```bash
tmux list-session
tmux ls
```
::::
:::: {.column width="50%"}
Dans une **session**

`Prefix + w`
::::
:::

### Se détacher d'une **session**

`Prefix + d`

> `<C-b>d` par défaut

### S'attacher à une **session** existante

```bash
tmux attach -t $session_name
tmux a -t $session_name
```

> `Prefix + w` est **interactif**

### :tada: Bonus :confetti_ball:

> S'attacher à une **session** si elle existe, la créer sinon

```bash
tmux new-session -A -s $session_name
```

## Fenêtres

Une **session** contient des **fenêtres**

### Manipulation

::: columns
:::: {.column width="50%"}
Création

`Prefix + c`
::::
:::: {.column width="50%"}
Suppression

`Prefix + &`
::::
:::

### Déplacements

::: columns
:::: {.column width="50%"}
Précédente

`Prefix + p`
::::
:::: {.column width="50%"}
Suivante

`Prefix + n`
::::
:::

## Panneaux

Une **fenêtre** contient des **panneaux**

### Séparations

::: columns
:::: {.column width="50%"}
Verticale

`Prefix + %`

> [Configurable](https://gitlab.com/pcoves/tmux/blob/master/.tmux.conf#L12-13)
::::
:::: {.column width="50%"}
Horizontale

`Prefix + "`

> [Configurable](https://gitlab.com/pcoves/tmux/blob/master/.tmux.conf#L9-10)
::::
:::

### Déplacements

::: columns
:::: {.column width="50%"}
Précédent

`Prefix + ;`
::::
:::: {.column width="50%"}
Suivant

`Prefix + o`
::::
:::

> Plus à ce propos par la suite

### :tada: Bonus :confetti_ball:

::: columns
:::: {.column width="50%"}
Zoom *toggle*

`Prefix + z`
::::
:::: {.column width="50%"}
De **panneau** à **fenêtre**

`Prefix + !`
::::
:::

## *Plugins*

Il est possible d'étendre [`TMux`][tmux] avec des *plugins*

::: columns
:::: {.column width="50%"}
[*Continuum*](https://github.com/tmux-plugins/tmux-continuum)

Sauvegarde régulièrement l'état de [`TMux`][tmux]
::::
:::: {.column width="50%"}
[*Resurrect*][resurrect]

Recharge l'état précédent de [`TMux`][tmux] au lancement
::::
:::

> [TMux Plugin Manager](https://github.com/tmux-plugins/tpm)

## Démonstration

<script id="asciicast-540228" src="https://asciinema.org/a/540228.js" async></script>

## Résumé

* [`TMux`][tmux] contient des **sessions**
* Une **session** contient des **fenêtres**
* Une **fenêtre** contient des **panneaux**
* Les *plugins* rendent [`TMux`][tmux] robuste aux redémarrages et déconnexions

# [`NVim`][nvim]

* Basé sur [`Vim`][vim]
* Extensible en [`lua`][lua]
* Fonctionnalités modernes
* Mais toujours en console

## :warning:

[`NVim`][nvim] évolue plus vite que certaines distributions.

Il est parfois nécessaire d'ajouter des dépôts pour avoir une version à jour.

:wave: **Debian** :wave: **Ubuntu** :wave:

## Configuration

La configuration de [`NVim`][nvim] va dans

`~/.config/nvim/init.lua`

> Un exemple accessible [ici](https://gitlab.com/pcoves/nvim/-/blob/main/.config/nvim/init.lua)

## *Splits*

Tout comme dans [`Vim`][vim]

### Création

::: columns
:::: {.column width="50%"}
Verticaux

```
:vsplit $path<CR>
:vsp $path<CR>
```
::::
:::: {.column width="50%"}
Horizontaux

```
:split $path<CR>
:sp $path<CR>
```
::::
:::

### Déplacements

<table>
    <tr><td></td><td>`<C-w>k` :arrow_up:</td><td></td></tr>
    <tr><td>:arrow_left: `<C-w>h`</td><td></td><td>`<C-w>l` :arrow_right:</td></tr>
    <tr><td></td><td>`<C-w>j` :arrow_down:</td><td></td></tr>
</table>

> Plus à ce propos par la suite

## *Plugins*

[`NVim`][nvim] c'est bien

[`NVim`][nvim] boosté aux *plugins*, c'est mieux

### [`Packer`][packer]

[`Packer`][packer] est un *plugin* **et** un gestionnaire de *plugins*

Il permet de déclarer les *plugins* désirés en [`lua`][lua]

> Un exemple accessible [ici](https://gitlab.com/pcoves/nvim/-/blob/main/.config/nvim/lua/plugins.lua)

### [`TreeSitter`][treesitter]

Configuration pour la **colloration syntaxique**

::: columns
:::: {.column width="50%"}
Ajout d'une syntaxe

```
:TSInstall $syntaxe<CR>
```
::::
:::: {.column width="50%"}
Mise à jour des syntaxes

```
:TSUpdate<CR>
```
::::
:::

> Une configuration accessible [ici](https://gitlab.com/pcoves/nvim/-/blob/main/.config/nvim/lua/setup/ts.lua)

### [`Mason`][mason]

Gestion des *linters*, *formatters* et **LSP**

::: columns
:::: {.column width="50%"}
Ajout d'une fonctionnalité

```
:MasonInstall $syntaxe<CR>
```
::::
:::: {.column width="50%"}
Mise à jour des Fonctionnalités

```
:Mason<CR>
```

Puis `U` quand [`Mason`][mason] a mis ses dépôts à jour
::::
:::

### [`LSP Config`][lsp-config]

* Autocompletion intelligente
* Renommage global
* Affichage de documentation
* Saut aux définitions

> Une configuration accessible [ici](https://gitlab.com/pcoves/nvim/-/blob/main/.config/nvim/lua/setup/lsp.lua)

## Résumé

* [`NVim`][nvim] est basé sur [`Vim`][vim]
* Dispose des fonctionnalités modernes d'un *IDE*
* Tout en restant au chaud dans la console
* Se configure de manière déclarative en *IaC*

# *Now, kiss*

Ils vont si bien ensemble

## Sessions [`NVim`][nvim]

[`Vim`][vim] l'a fait, [`NVim`][nvim] l'a conservé

### Nativement

::: columns
:::: {.column width="50%"}
Créer une session

```
:mksession<CR>
:mksession $file_name<CR>
```
::::
:::: {.column width="50%"}
Charger une session

```
nvim -S
nvim -S $file_name
```
::::
:::

### Automagiquement

L'**immense** [Tim POPE](https://github.com/tpope) propose le *plugin* [`Obsession`][obsession]

Il permet de mettre à jour automatiquement le fichier de session à chaque changement dans [`NVim`][nvim]

### Marions-les

* [`Resurrect`][resurrect] restore les **sessions** [`TMux`][tmux]
* [`Obsession`][obsession] restore les **sessions** [`NVim`][nvim]
* Les environnements de travail donc restaurés automagiquement à chaque lancement de la machine

## Déplacements

Se déplacer différement entre [`TMux`][tmux] et [`NVim`][nvim] est vite lassant.

> Chose promise, chose due.

### [`Navigator`][navigator]

Unifie les déplacements entre les deux

<table>
    <tr><td></td><td>`<C-k>` :arrow_up:</td><td></td></tr>
    <tr><td>:arrow_left: `<C-h>`</td><td></td><td>`<C-l>` :arrow_right:</td></tr>
    <tr><td></td><td>`<C-j>` :arrow_down:</td><td></td></tr>
</table>

> À travers [`TMux`][tmux] **et** [`NVim`][nvim]!

## :tada: Bonus :confetti_ball:

L'émulateur de **terminal** [`Alacritty`][alacritty] peut lancer [`TMux`][tmux] au démarrage.

```yaml
shell:
  program: bash
  args:
    - -l
    - -c
    - "tmux new-session -A -s TMux"
```

:gear: [`Alacritty`][alacritty] -> [`TMux`][tmux] -> [`Resurrect`][resurrect] -> [`Nvim`][nvim] :partying_face:

# Conclusion

[`TMux`][tmux] et [`NVim`][nvim] sont chacun des  outils **en console** extrêmement puissants.

Ensemble, ils décuplent leurs potentiels et apportent une nouvelle expérience de travail **sobre et efficace**.

Bien intégrés à l'émulateur de **terminal**, aucun effort n'est à fournir.

[tmux]: https://github.com/tmux/tmux/wiki
[nvim]: https://neovim.io/
[vim]: https://www.vim.org/
[resurrect]: https://github.com/tmux-plugins/tmux-resurrect
[lua]: https://www.lua.org/
[packer]: https://github.com/wbthomason/packer.nvim
[treesitter]: https://github.com/nvim-treesitter/nvim-treesitter
[mason]: https://github.com/williamboman/mason.nvim
[lsp-config]: https://github.com/neovim/nvim-lspconfig
[obsession]: https://github.com/tpope/vim-obsession
[navigator]: https://github.com/christoomey/vim-tmux-navigator
[alacritty]: https://alacritty.org/
